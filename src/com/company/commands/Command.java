package com.company.commands;

public abstract interface Command {

    public void execute();

}
