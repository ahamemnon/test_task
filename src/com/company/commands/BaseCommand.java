package com.company.commands;

public abstract class BaseCommand implements Command {

    protected String name;
    protected String description;

    public BaseCommand() {
        name = "";
        description = "";
    }

    public BaseCommand(String name, String description) {
        setName(name);
        setDescription(description);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

}
