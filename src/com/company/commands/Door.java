package com.company.commands;

public class Door {

    private boolean state;

    public Door(boolean state) {
        this.state = state;
    }

    public boolean isOpened() {
        return state;
    }

    public void open() {
        if (!isOpened()) {
            state = true;
        }
    }

    public void close() {
        if (isOpened()) {
            state = false;
        }
    }

}
