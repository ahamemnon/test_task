package com.company.commands;

public class DoorOpenCommand extends BaseCommand{

    private Door door;

    public DoorOpenCommand(Door door) {
        super();
        setDoor(door);
    }

    public DoorOpenCommand(String name, String description, Door door) {
        super(name, description);
        setDoor(door);
    }

    public Door getDoor() {
        return door;
    }

    public void setDoor(Door door) {
        this.door = door;
    }

    @Override
    public void execute() {
        door.open();
    }
}
