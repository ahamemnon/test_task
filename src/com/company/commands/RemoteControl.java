package com.company.commands;

import java.util.ArrayList;

public class RemoteControl {

    private ArrayList<BaseCommand> commands;

    public RemoteControl() {
        commands = new ArrayList<BaseCommand>();
    }

    public RemoteControl(ArrayList<BaseCommand> commands) {
        setCommands(commands);
    }

    public BaseCommand getCommand(int commandNumber) {
        return commands.get(commandNumber);
    }

    public ArrayList<BaseCommand> getCommands() {
        return commands;
    }

    public void setCommand(int commandNumber, BaseCommand command) {
        commands.set(commandNumber, command);
    }

    public void setCommands(ArrayList<BaseCommand> commands) {
        this.commands = commands;
    }

    public int getCommandsAmount() {
        return commands.size();
    }

    public void addCommand(BaseCommand command) {
        commands.add(command);
    }

    public void removeCommand(Command command) {
        commands.remove(command);
    }

    public void removeCommand(int commandNumber) {
        commands.remove(commandNumber);
    }

}
