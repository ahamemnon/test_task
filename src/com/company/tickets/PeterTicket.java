package com.company.tickets;

import com.company.exceptions.InvalidDigitPositionException;
import com.company.exceptions.InvalidTicketNumberException;

public class PeterTicket extends BaseTicket implements LuckyTicket {

    public PeterTicket(Integer number) throws InvalidTicketNumberException {
        super(number);
    }

    public int getEvenSum() throws InvalidDigitPositionException, InvalidTicketNumberException {
        return getDigit(1) + getDigit(3) + getDigit(5);
    }

    public int getOddSum() throws InvalidTicketNumberException, InvalidDigitPositionException {
        return getDigit(0) + getDigit(2) + getDigit(4);
    }

    @Override
    public boolean isLucky() throws InvalidDigitPositionException, InvalidTicketNumberException {
        validateNumber(number);
        return getEvenSum() == getOddSum();
    }

    @Override
    public int getLuckySum() throws InvalidDigitPositionException, InvalidTicketNumberException {
        return getOddSum();
    }

    @Override
    public String getLuckyString() throws InvalidTicketNumberException, InvalidDigitPositionException {
        String result = String.format("Счастливый билет (по-питерски) №%06d (%d+%d+%d = %d+%d+%d = %d)",
                getNumber(),
                getDigit(5),
                getDigit(3),
                getDigit(1),
                getDigit(4),
                getDigit(2),
                getDigit(0),
                getLuckySum());
        return result;
    }

}
