package com.company.commands;

public class Light {

    private boolean state;

    public Light(boolean state) {
        this.state = state;
    }

    public boolean isSwitchedOn() {
        return state;
    }

    public void switchOn() {
        if (!isSwitchedOn()) {
            state = true;
        }
    }

    public void switchOff() {
        if (isSwitchedOn()) {
            state = false;
        }
    }

}
