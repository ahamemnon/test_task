package com.company.exceptions;

public class InvalidTicketNumberException extends Exception {

    private int number;

    public InvalidTicketNumberException(int number) {
        super();
        this.number = number;
    }

    public String toString() {
        String result = "Invalid ticket number: " + Integer.toString(number)
                + ". The number must be in 0...999999";
        return result;
    }

}
